export default {
    newTodo: {
        name: '',
        completed: false
    },
    todos: [],
    notifications: []
}
