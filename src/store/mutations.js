export const saveTodos = (state, todos) => {
    state.todos = todos
}

export const addNotification = (state, notification) => {
    state.notifications.push(notification)
}

export const dismissNotification = (state, notification) => {
    state.notifications =
        state.notifications
            .map(n => {
                if (n.data.id === notification.id) {
                    clearTimeout(n.timeout)
                }

                return n
            })
            .filter(n => n.data.id !== notification.id)
}

export const updateTodo = (state, todo) => {
    state.todos =
        state.todos
            .map(t => t.id === todo.id ? todo : t)
}

export const removeTodo = (state, todo) => {
    state.todos = state.todos.filter(t => t.id !== todo.id)
}

export const updateNewTodo = (state, newTodo) => {
    state.newTodo = newTodo
}

export const addNewTodo = (state, todo) => {
    state.newTodo = {
        completed: false,
        name: ''
    }

    state.todos.push(todo)
}
