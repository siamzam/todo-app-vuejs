import * as api from '../api'

export const fetchTodos = ({commit, dispatch}) => {
    api.getAllTodos()
        .then(({data}) => {
            commit('saveTodos', data)
            if (data.length) {
                dispatch('addNotification',
                    {
                        type: 'success',
                        message: 'Loaded all the Todos!'
                    }
                )
            }
        })
        .catch(() => {
            dispatch('addNotification',
                {
                    type: 'error',
                    message: 'An error occured while fetching the todos!'
                }
            )
        })
}

export const updateTodo = ({commit, dispatch}, todo) => {
    api.updateTodo(todo)
        .then(({data}) => {
            commit('updateTodo', data)
            dispatch('addNotification',
                {
                    type: 'success',
                    message: 'Updated the todo!'
                }
            )
        })
        .catch(() => {
            dispatch('addNotification',
                {
                    type: 'error',
                    message: 'An error occured while updating the todo!'
                }
            )
        })
}

export const removeTodo = ({dispatch, commit}, todo) => {
    api.removeTodo(todo)
        .then(() => {
            commit('removeTodo', todo)
            dispatch('addNotification',
                {
                    type: 'success',
                    message: 'Deleted the todo!'
                }
            )
        })
        .catch(() => {
            dispatch('addNotification',
                {
                    type: 'error',
                    message: 'An error occured while deleting the todo!'
                }
            )
        })
}

export const addNotification = ({commit}, data) => {
    var notification = {
        ...data,
        id: Date.now()
    }

    var timeout = setTimeout(() => {
        commit('dismissNotification', notification)
    }, notification.duration || 2000)

    commit('addNotification', {
        data: notification,
        timeout
    })
}

export const updateNewTodo = ({commit}, newTodo) => {
    commit('updateNewTodo', newTodo)
}

export const addNewTodo = ({state, dispatch, commit}) => {
    if (state.todos.length >= 10) {
        dispatch('addNotification', {
            type: 'error',
            message: 'The list is full!'
        })

        return
    }

    var todo = {
        ...state.newTodo,
        name: state.newTodo.name.trim()
    }

    if (!todo.name.length) {
        dispatch('addNotification', {
            type: 'error',
            message: 'Todo name cannot be empty!'
        })

        return
    }

    api.addNewTodo(todo)
        .then(({data}) => {
            commit('addNewTodo', data)
            dispatch('addNotification',
                {
                    type: 'success',
                    message: 'Created the new todo!'
                }
            )
        })
        .catch(() => {
            dispatch('addNotification',
                {
                    type: 'error',
                    message: 'An error occured while creating the todo!'
                }
            )
        })
}
