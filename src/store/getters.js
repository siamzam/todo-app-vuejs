export const getNotifications = (state) => {
    return state.notifications.map(notification => notification.data)
}

export const getTodos = (state) =>
    state.todos.sort((a, b) => b.id - a.id)

export const getNewTodo = (state) =>
    state.newTodo

export const getPercentCompleted = (state) =>
    state.todos.filter(todo => todo.completed).length / state.todos.length * 100
