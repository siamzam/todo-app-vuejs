import axios from 'axios'

const BASE_URL = 'http://localhost:3000'

export const getAllTodos = () => axios.get(`${BASE_URL}/todos`)

export const updateTodo = (todo) => axios.put(`${BASE_URL}/todos/${todo.id}`, todo)

export const removeTodo = (todo) => axios.delete(`${BASE_URL}/todos/${todo.id}`)

export const addNewTodo = (todo) => axios.post(`${BASE_URL}/todos`, todo)
