import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import storeConfig from '../utils/store-config'
import * as api from '@/api'
import { cloneDeep } from 'lodash'

const localVue = createLocalVue()
localVue.use(Vuex)

jest.mock('@/api')

describe('Store', () => {
    let store

    const data = [
        {id: 1, name: 'Todo 1', completed: false},
        {id: 2, name: 'Todo 2', completed: true},
        {id: 3, name: 'Todo 3', completed: false}
    ]

    const state = {
        ...storeConfig.state,
        todos: data
    }

    beforeEach(() => {
        store = new Vuex.Store(cloneDeep({...storeConfig, state}))
    })

    it('removes a todo', async () => {
        api.removeTodo.mockResolvedValue({data: data[1]})

        await store.dispatch('removeTodo', data[1])

        expect(store.getters.getTodos)
            .toEqual([data[2], data[0]])
    })

    it('updates a todo', async () => {
        const payload = {
            ...data[0],
            completed: true
        }

        api.updateTodo.mockResolvedValue({data: payload})

        await store.dispatch('updateTodo', payload)

        expect(store.getters.getTodos)
            .toEqual(expect.arrayContaining([payload]))
    })

    it('adds a new todo', async () => {
        const payload = {
            id: 4,
            name: 'Todo 4',
            completed: true
        }

        store.commit('updateNewTodo', payload)

        api.addNewTodo.mockResolvedValue({data: payload})

        await store.dispatch('addNewTodo')

        expect(store.getters.getTodos)
            .toEqual(expect.arrayContaining([payload]))
    })
})
