import * as actions from '@/store/actions'
import * as api from '@/api'

jest.mock('@/api')

describe('Actions', () => {
    it('fetchTodos', async () => {
        const response = {
            data: [
                {id: 1, name: 'Todo 1', completed: false},
                {id: 2, name: 'Todo 2', completed: true},
                {id: 3, name: 'Todo 3', completed: false}
            ]
        }

        const commit = jest.fn()
        const dispatch = jest.fn()

        api.getAllTodos.mockResolvedValue(response)
        await actions.fetchTodos({commit, dispatch})

        expect(commit)
            .toHaveBeenCalledWith('saveTodos', response.data)

        expect(dispatch)
            .toHaveBeenCalledWith('addNotification',
                expect.objectContaining({type: 'success'})
            )
    })
})
