import * as mutations from '@/store/mutations'

describe('Mutations', () => {
    it('saveTodos', () => {
        const state = { todos: [] }

        const todos = [
            {id: 1, name: 'Todo 1', completed: false}
        ]

        mutations.saveTodos(state, todos)

        expect(state.todos)
            .toEqual(todos)
    })

    it('updateTodo', () => {
        const todos = [
            {id: 1, name: 'Todo 1', completed: false}
        ]

        const state = { todos }

        mutations.updateTodo(state,
            {id: 1, name: 'Todo 1', completed: true}
        )

        expect(state.todos[0].completed)
            .toBeTruthy()
    })

    it('removeTodo', () => {
        const todos = [
            {id: 1, name: 'Todo 1', completed: false}
        ]

        const state = { todos }

        mutations.removeTodo(state,
            {id: 1, name: 'Todo 1', completed: false}
        )

        expect(state.todos)
            .toEqual([])
    })
})
