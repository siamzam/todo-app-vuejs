import * as getters from '@/store/getters'

describe('Getters', () => {
    it('getTodos', () => {
        const todos = [
            {id: 1, name: 'Todo 1', completed: false},
            {id: 3, name: 'Todo 2', completed: true},
            {id: 2, name: 'Todo 3', completed: false}
        ]

        const state = { todos }

        getters.getTodos(state)

        expect(state.todos)
            .toEqual([
                {id: 3, name: 'Todo 2', completed: true},
                {id: 2, name: 'Todo 3', completed: false},
                {id: 1, name: 'Todo 1', completed: false}
            ])
    })

    it('getPercentCompleted', () => {
        const todos = [
            {id: 1, name: 'Todo 1', completed: false},
            {id: 2, name: 'Todo 2', completed: true}
        ]

        const state = { todos }

        expect(getters.getPercentCompleted(state))
            .toEqual(50)
    })
})
